#!/bin/bash
set -e

# Create SVN repository
echo " * Create SVN repository"
oc rsh dc/svn svnadmin create /var/svn/ || true
#
echo " * Add permission to account $LDAP_USER_TEST"
oc rsh dc/svn bash -c "echo -e '[/]\n$LDAP_USER_TEST = rw' >>/var/svn/conf/authz"
#
echo " * Getting hostname"
hostname=$(oc get route/cern-sso-proxy -o go-template --template '{{.spec.host}}')
#
echo " * List contents of the repository, must be empty"
svn ls "https://$hostname/repo" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST" --non-interactive
#
echo " * Checkout the repository"
svn co "https://$hostname/repo" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST" --non-interactive
#
echo " * Add file date"
cd repo
#
date >date.txt
#
svn add date.txt
#
echo " * Commiting the new file"
svn ci -m "Add test date" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST" --non-interactive
#
echo " * See that the contents are the expected"
test "$(svn ls "https://$hostname/repo" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST")" = "date.txt"
echo " * DONE *"
