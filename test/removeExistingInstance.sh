#!/bin/bash

set -e

# Remove all the elements created by the template to have a fresh start
oc delete -n "${CI_PROJECT}" deploymentconfig,route,service,configmap,serviceaccount,persistentvolumeclaim,CronJob -l template="${RESOURCE}"


# Wait until the pvc is completely deleted to proceed
while oc get pvc repository 2> /dev/null
do
    sleep 5s
done
