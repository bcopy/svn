---
# This template is intended for users in order to have a self contained SVN repository.
#
kind: "Template"
apiVersion: "v1"
metadata:
  name: "svn-server"
  annotations:
    description: "A standalone SVN server for teams who wish to run their own instance. No support is provided beyond security updates. See https://gitlab.cern.ch/vcs/subversion/svn"
    template.openshift.io/bindable: "false" # make sure users aren't proposed the Bind button in the service catalog UI
labels:
  template: "svn-server" #this label will be applied to all objects created from this template
objects:
  - kind: "Service"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
    spec:
      ports:
        - name: "8081-tcp"
          protocol: "TCP"
          port: 8081
          targetPort: 8081
      selector:
        name: "cern-sso-proxy"
      portalIP: ""
      type: "ClusterIP"
      sessionAffinity: "None"
  - kind: "Service"
    apiVersion: "v1"
    metadata:
      name: "svn"
    spec:
      ports:
        - name: "8000-tcp"
          protocol: "TCP"
          port: 8000
          targetPort: 8000
      selector:
        deploymentconfig: svn
      portalIP: ""
      type: "ClusterIP"
      sessionAffinity: "None"
  - kind: "Route"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
      labels:
        # This will take of the route registration
        cern.ch/sso-registration: Shibboleth
    spec:
      to:
        kind: "Service"
        name: "cern-sso-proxy"
      port:
        targetPort: 8081
      tls:
        termination: "edge"
        insecureEdgeTerminationPolicy: Redirect
  -
    kind: "ConfigMap"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
    data:
      # This configMap contains the configurable files a user has to provide to
      # define the proxy and authorization of the server.
      authorize.conf: |2
        # Make sure clients cannot fake authentication by injecting a X-Remote-User header
        RequestHeader unset X-Remote-User

        #Protected resources, if you need to protect specific urls please change the Location path
        <Location "/websvn">
          ShibRequestSetting requireSession 1
          AuthType shibboleth
          <RequireALL>
            Require valid-user
          </RequireALL>
          # Make the value of REMOTE_USER (the email address) available to the backend
          # application as HTTP header X-Remote-User
          #RequestHeader set X-Remote-User %{REMOTE_USER}e
          # Use the following instead to pass login name rather than email address
          RequestHeader set X-Remote-User %{ADFS_LOGIN}e
        </Location>
      proxy.conf: |2
        <Location "/">
          ProxyPreserveHost On
          ProxyPass http://${SVN_SERVICE_HOST}:${SVN_SERVICE_PORT}/
        </Location>
  -
    kind: "ConfigMap"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-shib"
    data:
      # The shibboleth2.xml configmap is empty by default so the one from the image
      # will be taken instead
      shibboleth2.xml: ""
  -
    kind: "ConfigMap"
    apiVersion: "v1"
    metadata:
      name: "httpd-conf"
    data:
      passwords: ""
      tokens.conf: |
        #<Location /repo>
        #  Order deny,allow
        #  Deny from all
        #  Allow from all
        #
        #  AuthType Basic
        #  AuthBasicProvider file
        #  AuthUserFile "/etc/httpd/conf.d/configurable/passwords"
        #  AuthName "Subversion at CERN"
        #  Require valid-user
        #</Location>
      repository.conf: |
        LogLevel warn

        ErrorLog /dev/stderr
        TransferLog /dev/stdout

        ServerName ${NAMESPACE}

        <Location /repo>
            Order deny,allow
            Deny from all
            Allow from all

            AuthType Basic
            AuthBasicProvider ldap
            AuthName "Subversion at CERN"
            AuthLDAPURL "ldaps://cerndc.cern.ch:636/OU=Users,OU=Organic Units,DC=cern,DC=ch?sAMAccountName?sub?(objectClass=*)"
            #
            AuthLDAPRemoteUserAttribute sAMAccountName
            # Authenticate using provided credentials
            AuthLDAPInitialBindAsUser on
            AuthLDAPInitialBindPattern (.+) "CN=$1,OU=users,OU=organic units,DC=cern,DC=ch"
            AuthLDAPSearchAsUser on
            AuthLDAPCompareAsUser on
            #
            Require valid-user
            DAV svn
            SVNPath /var/svn/
            Require valid-user
            AuthzSVNAccessFile /var/svn/conf/authz
        </Location>

        <Location /websvn>
            RewriteEngine On
            #Insert RemoteUser as env variable (from the frontend)
            RewriteCond %{HTTP:X-Remote-User} ^(.*)$
            RewriteRule .* - [E=REMOTE_USER:%1]
        </Location>

  -
    apiVersion: v1
    data:
      README: |
        In order to add files to this folder, please use the docroot ConfigMap
    kind: ConfigMap
    metadata:
      creationTimestamp: null
      name: docroot
  -
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: repository
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: ${VOLUME_SIZE}

  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      creationTimestamp: null
      generation: 15
      labels:
        app: svn
        name: svn
      name: svn
    spec:
      replicas: 1
      revisionHistoryLimit: 10
      selector:
        app: svn
        deploymentconfig: svn
      strategy:
        activeDeadlineSeconds: 21600
        resources: {}
        rollingParams:
          intervalSeconds: 1
          maxSurge: 25%
          maxUnavailable: 25%
          timeoutSeconds: 600
          updatePeriodSeconds: 1
        type: Rolling
      template:
        metadata:
          annotations:
            openshift.io/generated-by: OpenShiftNewApp
          creationTimestamp: null
          labels:
            app: svn
            deploymentconfig: svn
        spec:
          containers:
          - env:
            - name: NAMESPACE
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: metadata.namespace
            image: " "
            imagePullPolicy: IfNotPresent
            name: svn
            ports:
            - containerPort: 8000
              protocol: TCP
            resources:
              limits:
                cpu: "2"
                memory: 1Gi
              requests:
                cpu: 25m
                # an idle app uses just 8MB
                memory: 50Mi
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
            volumeMounts:
            - mountPath: /var/svn
              name: repository
            - mountPath: /var/www/html
              name: docroot
            - mountPath: /etc/httpd/conf.d/configurable/
              name: httpd-conf
          dnsPolicy: ClusterFirst
          restartPolicy: Always
          schedulerName: default-scheduler
          securityContext: {}
          terminationGracePeriodSeconds: 30
          volumes:
          - name: repository
            persistentVolumeClaim:
              claimName: repository
          - configMap:
              name: docroot
            name: docroot
          - configMap:
              defaultMode: 420
              name: httpd-conf
            name: httpd-conf
      test: false
      triggers:
      - type: ConfigChange
      - type: ImageChange
        imageChangeParams:
          automatic: true
          containerNames:
          - svn
          from:
            kind: ImageStreamTag
            name: svn-server:stable
            namespace: openshift
  -
    kind: "DeploymentConfig"
    apiVersion: "v1"
    metadata:
      name: "cern-sso-proxy"
    spec:
      strategy:
        type: "Rolling"
      triggers:
      - type: "ConfigChange"
      - type: "ImageChange"
        imageChangeParams:
          automatic: true
          containerNames:
          - "shibd"
          from:
            kind: "ImageStreamTag"
            name: "cern-sso-proxy:stable"
            namespace: openshift
      - type: "ImageChange"
        imageChangeParams:
          automatic: true
          containerNames:
          - "httpd"
          from:
            kind: "ImageStreamTag"
            name: "cern-sso-proxy:stable"
            namespace: openshift
      replicas: 1
      selector:
        name: "cern-sso-proxy"
      template:
        metadata:
          labels:
            name: "cern-sso-proxy"
        spec:
          containers:
            -
              name: "httpd"
              image: "cern-sso-proxy:stable"
              readinessProbe:
                failureThreshold: 3
                # use a short period so as not to slow down startup too much
                periodSeconds: 5
                successThreshold: 1
                tcpSocket:
                  port: 8081
                timeoutSeconds: 1
              resources:
                limits:
                  cpu: 250m
                  memory: 100Mi
                requests:
                  cpu: 25m
                  memory: 50Mi
              volumeMounts:
                # Shared mount for communication between both containers
              - mountPath: /var/run/shibboleth
                name: shared
                # Mount with apache configurable files
              - mountPath: /etc/httpd/conf.d/configurable
                name: apache
              - mountPath: /tmp/configmap
                name: shib
              env:
              -
                name: "NAMESPACE"
                valueFrom:
                  fieldRef:
                    apiVersion: v1
                    fieldPath: metadata.namespace
              -
                name: SERVICE_NAME
                value: "SVN"
              -
                name: HOSTNAME_FQDN
                value: ""
              terminationMessagePath: "/dev/termination-log"
              imagePullPolicy: "IfNotPresent"
              capabilities: {}
              securityContext:
                capabilities: {}
                privileged: false
            -
              name: "shibd"
              command: # Shibd container has a different entrypoint
              - /shib.sh
              image: "cern-sso-proxy:stable"
              resources:
                limits:
                  cpu: 250m
                  memory: 100Mi
                requests:
                  cpu: 25m
                  memory: 50Mi
              volumeMounts:
                # Shared mount for communication between both containers
              - mountPath: /var/run/shibboleth
                name: shared
              - mountPath: /tmp/configmap
                name: shib
              env:
              -
                name: "NAMESPACE"
                valueFrom:
                  fieldRef:
                    apiVersion: v1
                    fieldPath: metadata.namespace
              -
                name: HOSTNAME_FQDN
                value: ""
              terminationMessagePath: "/dev/termination-log"
              imagePullPolicy: "IfNotPresent"
              capabilities: {}
              securityContext:
                capabilities: {}
                privileged: false
          volumes:
            -
              emptyDir: {}
              name: shared
            - configMap:
                name: cern-sso-proxy
              name: apache
            - configMap:
                name: cern-sso-shib
              name: shib
          restartPolicy: "Always"
          dnsPolicy: "ClusterFirst"
  -
    kind: "CronJob"
    apiVersion: "batch/v1beta1"
    metadata:
      name: "sync-egroups"
    spec:
      concurrencyPolicy: Replace
      failedJobsHistoryLimit: 1
      jobTemplate:
        metadata:
          creationTimestamp: null
        spec:
          activeDeadlineSeconds: 600
          template:
            metadata:
              creationTimestamp: null
            spec:
              restartPolicy: Never
              containers:
              - command:
                - sync-egroups.sh
                - /var/svn/conf/authz
                image: docker-registry.default.svc:5000/openshift/svn-server:stable
                imagePullPolicy: Always
                name: sync-egroups
                resources: {}
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
                volumeMounts:
                - mountPath: /var/svn
                  name: repository
              dnsPolicy: ClusterFirst
              restartPolicy: Never
              schedulerName: default-scheduler
              securityContext: {}
              terminationGracePeriodSeconds: 30
              volumes:
              - name: repository
                persistentVolumeClaim:
                  claimName: repository
      schedule: 35 * * * *
      successfulJobsHistoryLimit: 3
      suspend: false
parameters:
  -
    name: "VOLUME_SIZE"
    description: "The size of the volume that will hold the Subversion repository files."
    value: 9Gi
